variable "fqdn" {
  description = "The fully-qualified domain name of the resulting S3 website."
  default     = "minions.jamtartley.com"
}

variable "domain" {
  description = "The hosted zone name"
  default     = "jamtartley.com"
}

variable "allowed_ips" {
  type = list(string)
  default = [
    "0.0.0.0/0"
  ]
}

variable "gitlab_token" {
  type        = string
  description = "Personal access token for Gitlab"
	sensitive = true
}