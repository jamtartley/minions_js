# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/anschoewe/curl" {
  version     = "0.1.3"
  constraints = "0.1.3"
  hashes = [
    "h1:MxSuRVxqpLiHPcLQUvtFLpm7mu9NXHyIuB0hSyxMYkE=",
    "zh:0055cac79b44edcf9498322861a470fe3b4209c802efbedb141b5914e2d6d4e1",
    "zh:0ec6e3972291892253dc92ecb9dc59eb4b62042cdaa35079e0a0093c4513f634",
    "zh:11fa58f842c1350dd8a8078b415358a9ac1648e0b01f6c94c9edaa8522a41296",
    "zh:1ffa9f67ef00dcebf512b595e970c13b01a43bab7287284100ef37bd01f41d98",
    "zh:20b83d2952dec93b5ebfd37f6dbd0c825dd5f788b1f15dd5ce9488ffa9758785",
    "zh:40792495f666461d317cd76ef4a34c92cb22a489da1bdf264e4f8c9f046e625b",
    "zh:555b1d4c8c8fd9af15076ab285827ea997e7177bb3231013944f72cae4995761",
    "zh:591532fbf8431c8ef672bcc4993ad55242f0bb2786d05bf0c97f9a32286cc3f1",
    "zh:b54c8eebf3f4f36ccc4cd063938ddceca6218d2731e56f75c3b6520329ec66a3",
    "zh:de80e822aa2f6ab79c5ba1e9ac1422c256931c5365b10490ed0d88de104cab8e",
    "zh:e041084353252c5135133e8aaf3f19919a0cdaaaa924c9b2ca7aed8bb25df50f",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.7.0"
  constraints = "3.7.0"
  hashes = [
    "h1:Xe8KHFOG3zA61RjvDtVgWKCPeT4l1++XEGVJyNEeeM4=",
    "zh:16addba6bda82e6689d21049392d296ce276cb93cbc5bcf3ad21f7d39cd820fd",
    "zh:1e9dd3db81a38d3112ddb24cec5461d919e7770a431f46ac390469e448716fd4",
    "zh:252c08d473d938c2da2711838db2166ecda2a117f64a10d279e935546ef7d991",
    "zh:2e0c83da0ba44e0521cb477dd60ea7c08b9edbc44b607e5ddb963becb26970a5",
    "zh:396223391782f1f809a60f045bfdcde41820d0d6119912718d86fc153fc28969",
    "zh:3a6b3c0901b81bc451d1ead2a2f26845d5db6b6253758c1f0aa0bad53fb6b4bd",
    "zh:51010e8f1d05f4979f0e10cf0e3b56cec13c731d99f373dda9fd9561ddb2826b",
    "zh:53ef55edf7698cbb4562265a6ab9e261716f62a82590e5fb6ad4f7f7458bdc5c",
    "zh:6c2db10e6226cc748e6dd5c1cbc257fda30cd58a175a32fc95a8ccd5cebdd3e7",
    "zh:91627f5af7e8315479a6c45cb1ae5db3c0a91a18018383cd409f3cfa04408aed",
    "zh:b5217a81cfc58334278831eacb2865bd8fc025b0cb1c576e9da9c4dc3a187ef5",
    "zh:c70afea4324518b099d23abc189dff22e6706ca0936de39eca01851e2550af7e",
    "zh:e62c212169ef9aad3b01f721db03b7e08d7d4acbbac67a713f06239a3a931834",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.65.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:ru8cxkvHaLrVIE0hSDyO4npDIPpHsKr7z8jMwLvvu3U=",
    "zh:108aeaf5e18087d9ac852737a5be1347a28e40825817cc1a29ec523d40268294",
    "zh:1a719c0c9754f906b2220d3bbf90d483ec0a74cf87768a464d2d657b7901ec6b",
    "zh:21acdc35ae70a626cbc81eff06181a78843f1ddc2d9200f80fabf2e0466ecbda",
    "zh:28846628e1a4227a1f2db256d6b22ed36922f37632999af7404aa74703cd9bfb",
    "zh:32455550dbf86ae07d9782650e86d23c4fa13d7872e48680044692894e8da6ea",
    "zh:4241246274627c752f9aef2806e810053306001e80fc5b51d27cbe997f75f95e",
    "zh:5ca0fab3ceb3f41a97c1ebd29561a034cb83fda04da35fd5f8c3c5cb97bb3ea8",
    "zh:5fed3b79d4ed6424055e8bbfb7a4393e8db5102cdba04b4590f8e0f4194637fb",
    "zh:99a0bc325b0a59ded1152546c004953a2bb0e110978bf0cc55e1804384941bdb",
    "zh:e74f9190a417c891992210f9af937ef55749d86a04762d982260fbbc989342a7",
    "zh:fb6984405ca63d0373bd992ce157e933b8ae9dd94d74b1c5691632f062fe60b2",
  ]
}
