module.exports = {
	arrowParens: "avoid",
	bracketSpacing: true,
	endOfLine: "lf",
	htmlWhitespaceSensitivity: "css",
	bracketSameLine: false,
	printWidth: 120,
	proseWrap: "preserve",
	requirePragma: false,
	semi: true,
	singleQuote: false,
	trailingComma: "es5",
	useTabs: true,
};
